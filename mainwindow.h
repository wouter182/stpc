#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QLegend>
#include <QtCharts/QLineSeries>
#include <QtCharts/QCategoryAxis>

#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QHorizontalStackedBarSeries>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>

#include <QtCore>
#include <QtGui>
#include <QWidget>
#include <QtCharts>
#include <QtSvg/QSvgGenerator>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    std::vector<double> X = {0};
    std::vector<double> Y = {0};

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void about();
    void exit();
    void exportSVG();
    void exportPNG();
    QChart *plotFigure();

    void on_calculateState_clicked();

private:
    Ui::MainWindow *ui;
    QChart *chartToPlot;
    QChartView *chartViewExport;
    QChart chartToExport;

};

#endif // MAINWINDOW_H
