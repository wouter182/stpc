#include "calculus.h"
#include <cmath>
#include <vector>

std::vector<double> cubicFunction(double a, double b, double c, double d)
{
    std::vector<double> xvec;
    std::vector<double> tvec;
    double p = 0.0;
    double q = 0.0;
    double D = 0.0;
    double A = 0.0;
    double theta = 0.0;
    double t = 0.0;
    double pi = 3.141592654;
    double x = 0.0;

    // Cubic function a*x^3+b*x^2+c*x+d=0
    p = (3*a*c-pow(b,2))/(3*pow(a,2));
    //std::cout << "p :" << std::scientific << p << std::endl;
    q = (2*pow(b,3)-9*a*b*c+27*pow(a,2)*d)/(27*pow(a,3));
    //std::cout << "q :" << q << std:: endl;
    D = -4*pow(p,3)-27*pow(q,2);
    //std::cout << "D :" << D << std::endl;

    if (D >= 0)
    {
        for(int i = 0; i < 3; i++)
        {
            // There are three unequal real roots, p<0
            A       = 2.0*std::sqrt(-p/3.0);
            theta   = std::acos(3.0*q/(2.0*p)*std::sqrt(-3.0/p));
            t       = A*std::cos(1.0/3.0*(theta+2.0*pi*i));
            x       = t-b/(3.0*a);
            tvec.push_back(t);
            xvec.push_back(x);
        }
    }
    // D <0, one root is real and two complex conjugate roots;
    else if (p < 0)
    {
        A       = -2.0*std::abs(q)/q*std::sqrt(-p/3.0);
        theta   = std::acosh(-3.0*std::abs(q)/(2.0*p)*std::sqrt(-3.0/p));
        t       = A*std::cosh(theta/3.0);
        x       = t-b/(3.0*a);
        xvec.push_back(x);
    }
    else
    {
        A     = -2.0*std::sqrt(p/3.0);
        theta = std::asinh(3.0*q/(2.0*p)*std::sqrt(3.0/p));
        t     = A*std::sinh(theta/3.0);
        x     = t-b/(3.0*a);
        xvec.push_back(x);
    }
    return xvec;
}
