/*---------------------------------------------------------------------------*\
STPC
-------------------------------------------------------------------------------
License


Class
    Foam::pengrobinson

Description
    Something.

SourceFiles
    pengRobinson.C

\*---------------------------------------------------------------------------*/

#ifndef PENGROBINSON_H
#define PENGROBINSON_H

#include <iostream>
#include <cmath>
#include <vector>


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace STPC
{

/*---------------------------------------------------------------------------*\
                           Class cubic Declaration
\*---------------------------------------------------------------------------*/

class pengRobinson
{
protected:

    // Protected data

private:

    // Private Data
    //- Variables
    double p;
    double T;
    double p_c;
    double T_c;
    double omega;
    double p_r;
    double T_r;
    double R;
    double V;
    double a_PR;
    double b_PR;
    double alpha_PR;
    double kappa_PR;

public:
    //- Constructor
    pengRobinson()
    {
        p           = 0; // [Pa]
        T           = 0; //
        p_c         = 22.0640e6; //[Pa]
        T_c         = 647.096; //[K]
        omega       = 0.3443; //[-]
        p_r         = 0;
        T_r         = 0;
        V           = 0;
        R           = 8.3144598; //[J/(mol*K)]
        a_PR        = 0;
        b_PR        = 0;
        alpha_PR    = 0;
        kappa_PR    = 0;
    }

    //- Destructor
    ~pengRobinson()
    {}

    struct phaseType {double liquid; double gas;};
    struct thermodynamicProperties {phaseType V; phaseType Z; phaseType Hdep; phaseType Sdep;};

    // Member functions declaration
    void setPressure(double pressure);
    void setTemperature(double temperature);
    void setPressureCritical(double pressureCritical);
    void setTemperatureCritical(double temperatureCritical);
    void setOmega(double omega_temp);
    double getPressure();
    double getTemperature();
    double getTemperatureCritical();
    double getPressureCritical();
    double getOmega();
    double getPressureReduced();
    double getTemperatureReduced();
    auto pengRobinsonEOSValues(double p_c, double T_c, double omega, double T);
    phaseType getVolume(double p, double T);
    phaseType getCompressibility(double p, double T);
    phaseType getDepartureEnthalpy(double T, phaseType Z, phaseType V);
    phaseType getDepartureEntropy(double T, phaseType Z, phaseType V);
    thermodynamicProperties calculateAllThermodynamicProperties(double p, double T);
    double getDeltaEntropyIdeal(double p1, double p2, double T1, double T2);
    double gammaPerfectIdeal(double T1);
    double getTotalDeltaEntropy(double p1, double p2, double T1, double T2);
    double getIsentropicProcess(double p1, double p2, double T1);

};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace STPC

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif // PENGROBINSON_H

// ************************************************************************* //
