/*---------------------------------------------------------------------------*\
STPC
-------------------------------------------------------------------------------
License


Class
    Foam::pengrobinson

Description
    Something.

SourceFiles
    pengRobinson.C

\*---------------------------------------------------------------------------*/

#ifndef CALCULUS_H
#define CALCULUS_H

#include <iostream>
#include <cmath>
#include <vector>

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //



std::vector<double> cubicFunction(double a, double b, double c, double d);


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif // CALCULUS_H

// ************************************************************************* //
