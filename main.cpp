#include "mainwindow.h"
#include "pengRobinson.h"
#include "calculus.h"
#include <QApplication>
#include <iostream>

int main(int argc, char *argv[])
{
    QApplication STPC(argc, argv);
    MainWindow w;
    w.show();

    //std::cout << "C++ version: " << __cplusplus << std::endl;

    return STPC.exec();
}
