/*---------------------------------------------------------------------------*\
STPC
-------------------------------------------------------------------------------
License
    Something
\*---------------------------------------------------------------------------*/

#include "pengRobinson.h"
#include "calculus.h"
#include "bits/stdc++.h"
#include <iostream>
#include <cmath>
#include <vector>

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace STPC
{
    void pengRobinson::setPressure(double pressure)
    {
        p = pressure;
    }

    void pengRobinson::setTemperature(double temperature)
    {
        T = temperature;
    }

    void pengRobinson::setPressureCritical(double pressureCritical)
    {
        p_c = pressureCritical;
    }

    void pengRobinson::setTemperatureCritical(double temperatureCritical)
    {
        T_c = temperatureCritical;
    }

    void pengRobinson::setOmega(double omega_temp)
    {
        omega = omega_temp;
    }

    double pengRobinson::getPressure()
    {
        std::cout << "Pressure: " << p << " [pa]" << std::endl;
        return p;
    }

    double pengRobinson::getTemperature()
    {
        std::cout << "Temperature: " << T << " [K]" << std::endl;
        return T;
    }

    double pengRobinson::getPressureCritical()
    {
        std::cout << "Critical pressure: " << p_c << " [Pa]" << std::endl;
        return p_c;
    }

    double pengRobinson::getTemperatureCritical()
    {
        std::cout << "Critical temperature: " << T_c << " [K]" << std::endl;
        return T_c;
    }

    double pengRobinson::getOmega()
    {
        std::cout << "Omega: " << omega << " [-]" << std::endl;
        return omega;
    }

    double pengRobinson::getPressureReduced()
    {
        p_r = p/p_c;
        std::cout << "Reduced pressure: " << p_r << " [-]" << std::endl;
        return T_r;
    }

    double pengRobinson::getTemperatureReduced()
    {
        T_r = T/T_c;
        std::cout << "Reduced temperature: " << T_r << " [-]" << std::endl;
        return T_r;
    }

    auto pengRobinson::pengRobinsonEOSValues(double pressureCritical, double temperatureCritical, double omega_temp, double temperature)
    {
        double a        = 0.45724*std::pow(R*temperatureCritical,2)/pressureCritical; // [Pa*m^6/mol^4]
        double b        = 0.07780*std::pow(R*temperatureCritical,1)/pressureCritical; // [Pa*m^3/mol^2]
        double kappa    = 0.37464+1.54226*omega_temp-0.26992*std::pow(omega_temp,2); // [#]
        double T_r      = temperature/T_c; // [#]
        double alpha    = std::pow(1+kappa*(1-std::sqrt(T_r)),2); // [#]
        struct PR {double a; double b; double kappa; double alpha; double T_r;};
        return PR {a, b, kappa, alpha, T_r};
    }

    STPC::pengRobinson::phaseType pengRobinson::getVolume(double p, double T)
    {
        std::vector<double> Vvec;

        auto PR = pengRobinsonEOSValues(p_c, T_c, omega, T);

        double b_cubic  = PR.b-R*T/p;
        double c_cubic  = -pow(PR.b,2)+PR.alpha*PR.a/p-2*pow(PR.b,2)-2*PR.b*R*T/p;
        double d_cubic  = -PR.alpha*PR.a*PR.b/p+pow(PR.b,2)*R*T/p+pow(PR.b,3);
        Vvec            = cubicFunction(1, b_cubic, c_cubic, d_cubic);
        double Vliquid  = *std::min_element(Vvec.begin(), Vvec.end());
        //std::cout << "V Liquid: " << Vliquid << " [m^3/mol]" << std::endl;
        double VGas     = *std::max_element(Vvec.begin(), Vvec.end());
        //std::cout << "V Gas: " << VGas << " [m^3/mol]" << std::endl;

        return phaseType {Vliquid, VGas};
    }

    STPC::pengRobinson::phaseType pengRobinson::getCompressibility(double p, double T)
    {
        std::vector<double> Zvec;

        auto PR = pengRobinsonEOSValues(p_c, T_c, omega, T);

        double A        = PR.alpha*PR.a*p/std::pow(R*T,2);
        double B        = PR.b*p/(R*T);
        double b_cubic  = -1.0*(1.0-B);
        double c_cubic  = A-2.0*B-3.0*std::pow(B,2);
        double d_cubic  = -1.0*(A*B-std::pow(B,2)-std::pow(B,3));

        Zvec            = cubicFunction(1, b_cubic, c_cubic, d_cubic);
        double ZLiquid  = *std::min_element(Zvec.begin(), Zvec.end());
        //std::cout << "Z min: " << ZLiquid << " [#]" << std::endl;
        double ZGas     = *std::max_element(Zvec.begin(), Zvec.end());
        //std::cout << "Z Gas: " << ZGas << " [#]" << std::endl;

        return phaseType {ZLiquid, ZGas};
    }

    STPC::pengRobinson::phaseType pengRobinson::getDepartureEnthalpy(double T, phaseType Z, phaseType V)
    {
        std::vector<double> Hdepvec;
        double Vtemp, Ztemp, Hdep;

        std::vector<double> Vvec    = {V.liquid, V.gas};
        std::vector<double> Zvec    = {Z.liquid, Z.gas};

        auto PR = pengRobinsonEOSValues(p_c, T_c, omega, T);

        for(int i = 0; i < 2; i++)
        {
            Vtemp       = Vvec[i];
            Ztemp       = Zvec[i];
            Hdep        = R*T*(Ztemp-1.0)+(PR.a*std::log((-1.0*std::pow(2.0,3.0/2.0)*PR.b+2.0*PR.b+2.0*Vtemp)/(std::pow(2.0,3.0/2.0)*PR.b+2.0*PR.b+2.0*Vtemp))*std::pow((1.0-std::sqrt(PR.T_r))*PR.kappa+1.0,2.0))/(std::pow(2.0,3.0/2.0)*PR.b)+T*((PR.a*std::log((-1.0*std::pow(2.0,3.0/2.0)*PR.b+2.0*PR.b+2.0*Vtemp)/(std::pow(2.0,3.0/2.0)*PR.b+2.0*PR.b+2.0*Vtemp))*PR.kappa*((1.0-std::sqrt(PR.T_r))*PR.kappa+1.0))/(std::pow(2.0,3.0/2.0)*std::sqrt(PR.T_r)*T_c*PR.b)+R*std::log(Vtemp-PR.b))-R*T*std::log(Vtemp-PR.b); //[J/mol]
            //std::cout << "H dep: " << Hdep << " [J/mol]" << std::endl;
            Hdepvec.push_back(Hdep);
        }
        double HdepLiquid   = *std::min_element(Hdepvec.begin(), Hdepvec.end());
        //std::cout << "H Liquid: " << HdepLiquid << " [J/mol]" << std::endl;
        double HdepGas      = *std::max_element(Hdepvec.begin(), Hdepvec.end());
        //std::cout << "H Gas: " << HdepGas << " [J/mol]" << std::endl;

        return phaseType {HdepLiquid, HdepGas}; //[J/mol]
    }

    STPC::pengRobinson::phaseType pengRobinson::getDepartureEntropy(double T, phaseType Z, phaseType V)
    {
        std::vector<double> Sdepvec;
        double Sdep, Vtemp, Ztemp;

        std::vector<double> Vvec    = {V.liquid, V.gas};
        std::vector<double> Zvec    = {Z.liquid, Z.gas};

        auto PR = pengRobinsonEOSValues(p_c, T_c, omega, T);

        for(int i = 0; i < 3; i++)
        {
            Vtemp       = Vvec[i];
            Ztemp       = Zvec[i];

            Sdep        = R*std::log((Vtemp-PR.b)/Vtemp)+(PR.a*std::log((-1.0*std::pow(2,3.0/2.0)*PR.b+2.0*PR.b+2.0*Vtemp)/(std::pow(2.0,3.0/2.0)*PR.b+2.0*PR.b+2.0*Vtemp))*PR.kappa*((1.0-std::sqrt(PR.T_r))*PR.kappa+1.0))/(std::pow(2,3.0/2.0)*std::sqrt(PR.T_r)*T_c*PR.b)+R*std::log(Ztemp); //[J/(mol)]
            //std::cout << "S dep: " << Sdep << " [J/(mol*K)]" << std::endl;
            Sdepvec.push_back(Sdep);
        }
        double SdepLiquid   = *std::min_element(Sdepvec.begin(), Sdepvec.end());
        //std::cout << "S Liquid: " << SdepLiquid << " [J/(mol*K)]" << std::endl;
        double SdepGas      = *std::max_element(Sdepvec.begin(), Sdepvec.end());
        std::cout << "S Gas: " << SdepGas << " [J/(mol*K)]" << std::endl;

        return phaseType {SdepLiquid, SdepGas}; //[J/(mol*K)]
    }

    double pengRobinson::gammaPerfectIdeal(double T1)
    {
        double t1;
        double cp;
        double cv;
        double gamma;

        double A_cp = 32.24; //30.09200; //[J/(mol*K)] NIST
        double B_cp = 1.92E-03; //6.832514; //[J/(mol*K^2)] NIST
        double C_cp = 1.06E-05; //6.793435; //[J/(mol*K^3)] NIST
        double D_cp = -3.60E-09; //-2.534480; //[J/(mol*K^4)] NIST
        double E_cp = 0; //0.082139; //[J*K/mol] NIST

        t1    = T1;// /1000; //[K]
        cp    = A_cp+B_cp*t1+C_cp*std::pow(t1,2.0)+D_cp*std::pow(t1,3.0)+E_cp*std::pow(t1,-2.0); //[J/(mol*K)]
        cv    = cp-R; //[J/(mol*K)]
        gamma = cp/cv; //[#]

        return gamma; //[#]
    }

    double pengRobinson::getDeltaEntropyIdeal(double p1, double p2, double T1, double T2)
    {
        double DSIdeal;
        double A_cp = 32.24; //30.09200; //[J/(mol*K)] NIST
        double B_cp = 1.92E-03; //6.832514; //[J/(mol*K^2)] NIST
        double C_cp = 1.06E-05; //6.793435; //[J/(mol*K^3)] NIST
        double D_cp = -3.60E-09; //-2.534480; //[J/(mol*K^4)] NIST
        double E_cp = 0; //0.082139; //[J*K/mol] NIST
        double t1   = T1;// /1000; //[K]
        double t2   = T2;// /1000; //[K]
        DSIdeal     = A_cp*std::log(t2/t1)+B_cp*(t2-t1)+1/2.0*C_cp*(std::pow(t2,2.0)-std::pow(t1,2.0))+1/3.0*D_cp*(std::pow(t2,3.0)-std::pow(t1,3.0))-1/2.0*E_cp*(std::pow(t2,-2.0)-std::pow(t1,-2.0))-R*log(p2/p1); //[J/(mol*K)]

        std::cout << "DSIdeal: " << DSIdeal << std::endl;
        //std::cout << "t1: " << t1 << std::endl;
        //std::cout << "t2: " << t2 << std::endl;
        //std::cout << "p1: " << p1 << std::endl;
        //std::cout << "p2: " << p2 << std::endl;

        return DSIdeal; //[J/(mol*K)]
    }

    STPC::pengRobinson::thermodynamicProperties pengRobinson::calculateAllThermodynamicProperties(double p, double T)
    {
        phaseType V;
        phaseType Z;
        phaseType Hdep;
        phaseType Sdep;

        V       = getVolume(p, T);
        Z       = getCompressibility(p, T);
        Hdep    = getDepartureEnthalpy(T, Z, V);
        Sdep    = getDepartureEntropy(T, Z, V);

       return thermodynamicProperties {V, Z, Hdep, Sdep};
    }

    double pengRobinson::getTotalDeltaEntropy(double p1, double p2, double T1, double T2)
    {
        double DSIdeal;
        double DSTotal;
        double Sdep1;
        double Sdep2;


        DSIdeal = getDeltaEntropyIdeal(p1, p2, T1, T2);
        auto fluid1 = calculateAllThermodynamicProperties(p1, T1);
        Sdep1   = fluid1.Sdep.gas;
        std::cout << "Sdep1 max: " << Sdep1 << " [J/(mol*K)]" << std::endl;

        auto fluid2 = calculateAllThermodynamicProperties(p2, T2);
        Sdep2   = fluid2.Sdep.gas;
        std::cout << "Sdep2 max: " << Sdep2 << " [J/(mol*K)]" << std::endl;

        DSTotal = Sdep2 + DSIdeal - Sdep1; //[J/(mol*K)]

        std::cout << "DSTotal: " << DSTotal << std::endl;

        return DSTotal; //[J/(mol*K)]
    }

    double pengRobinson::getIsentropicProcess(double p1, double p2, double T1)
    {
        double DSTotal[40];
        double gamma;
        double T[40];
        double T2 = 0;

        gamma   = gammaPerfectIdeal(T1);
        T[0]    = std::pow(p2/p1,(gamma-1)/gamma)*T1;
        std::cout << "T[0]: " << T[0] << std::endl;
        DSTotal[0] = getTotalDeltaEntropy(p1, p2, T1, T[0]);
        std::cout << "DSTotal[0]: " << DSTotal[0] << std::endl;

        for (int i = 0; i < 40; i++)
        {
            std::cout << "i: " << i << std::endl;

            if (std::abs(DSTotal[i])<1e-14)
            {
                std::cout << "STOP" << std::endl;
                T2 = T[i];
                break;
            }
            else if (i==1)
            {
                T[i+1] = T[i]*1.01;
            }
            else
            {
                double f    = DSTotal[i];
                double dfdx = (DSTotal[i]-DSTotal[i-1])/(T[i]-T[i-1]);
                T[i+1]      = T[i] - f/dfdx;
                std::cout << "T[i]: " << T[i] << std::endl;
            }
            DSTotal[i+1]    = getTotalDeltaEntropy(p1, p2, T1, T[i+1]);
            std::cout << "DSTotal[i]: " << DSTotal[i+1] << std::endl;
        }
        std::cout << "T2: " << T2 << std::endl;
        return T2;
    }
}
// ************************************************************************* //
