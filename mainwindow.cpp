#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "pengRobinson.h"

#include <QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QLegend>
#include <QtCharts/QLineSeries>
#include <QtCharts/QCategoryAxis>

#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QHorizontalStackedBarSeries>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>

#include <QChart>
#include <QMessageBox>
#include <QTextStream>
#include <iostream>
#include <QtSvg/QSvgGenerator>
#include <QChartView>
#include <QGraphicsItem>
#include <QtCore>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->chartView->setRenderHint(QPainter::Antialiasing);
    QChart *chartToPlot = plotFigure();

    ui->chartView->setChart(chartToPlot);
    connect(ui->actionPrint, &QAction::triggered, this, &MainWindow::exportSVG);
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::exit);
    connect(ui->actionAbout, &QAction::triggered, this, &MainWindow::about);

}

MainWindow::~MainWindow()
{
    delete ui;
}

QChart *MainWindow::plotFigure()
{
    // Add data for the plot
    QLineSeries *series = new QLineSeries();
    series->setName("Serie 1");
    int length = X.size();
    for(int i = 0; i < length; i++)
    {
        //std::cout << "X: " << X[i] << std::endl;
        //std::cout << "Y: " << Y[i] << std::endl;
        series->append(X[i], Y[i]);
    }

    // Create a chart
    QChart *chart = new QChart();
    // Add data to the chart
    chart->addSeries(series);

    // Add chart style information
    QFont font;
    font.setPointSize(18);
    chart->setTitleFont(font);
    chart->setTitle("Temperature versus pressure");
    chart->legend()->hide();
    chart->setBackgroundBrush(Qt::white);
    chart->setAcceptHoverEvents(true);
    //chart->setTheme(QChart::ChartThemeBlueCerulean);

    // Create Axis
    QFont labelFont;
    labelFont.setPointSize(14);
    // X axis
    QValueAxis *axisX = new QValueAxis();
    chart->addAxis(axisX, Qt::AlignBottom);
    axisX->setTitleText("Pressure [Pa]");
    axisX->setGridLineVisible(true);
    axisX->setLabelFormat("%.2g");
    series->attachAxis(axisX);
    // Y axis
    QValueAxis *axisY = new QValueAxis();
    chart->addAxis(axisY, Qt::AlignLeft);
    axisY->setTitleText("Temperature [K]");
    axisY->setGridLineVisible(true);
    axisY->setLabelFormat("%.2f");
    series->attachAxis(axisY);

    return chart;
}

void MainWindow::exportSVG()
{
    QString fileName;
    fileName = QFileDialog::getSaveFileName(this, tr("Export file name"), QString(),"Graphic file ( *.svg)");

    // The desired size of the rendering
    // in pixels for PNG, in pt for SVG
    QSize output_size = QSize(800,600);
    QRectF output_rect = QRectF(QPointF(0,0), QSizeF(output_size)); //cast to float

    QChart *chartToExport = new QChart();
    chartToExport = plotFigure();

    if ( !fileName.isEmpty() )
    {
        QSvgGenerator generator;
        generator.setFileName(fileName);
        generator.setTitle(tr("SVG Generator"));
        generator.setDescription(tr("A SVG plot"));
        generator.setSize(output_size);
        generator.setViewBox(output_rect);

        //QSizeF original_size = chartToExport->size();
        chartToExport->resize(output_rect.size());

        QGraphicsScene *m_scene = new QGraphicsScene();
        m_scene->addItem(chartToExport);

        QPainter painter;
        painter.begin(&generator);
        painter.setRenderHint(QPainter::Antialiasing);
        m_scene->render(&painter, output_rect, output_rect, Qt::IgnoreAspectRatio);
        painter.end();
    }
}

void MainWindow::exportPNG()
{
    QString fileName;
    fileName = QFileDialog::getSaveFileName(this, tr("Export file name"), QString(),"Graphic file ( *.png)");

    if ( !fileName.isEmpty() )
    {
        QPixmap pixmap= QPixmap::grabWidget(this);
    }
}

void MainWindow::about()
{
   QMessageBox::about(this, tr("About STPC"),
                tr("The <b>Notepad</b> example demonstrates how to code a basic "
                   "text editor using QtWidgets"));
}

void MainWindow::exit()
{
    QCoreApplication::quit();
}

void MainWindow::on_calculateState_clicked()
{
    double p1       = ui->p1->value(); //[Pa]
    double p2       = ui->p2->value(); //[Pa]
    double T1       = ui->T1->value(); //[K]
    double T2       = 0; //[K]

    STPC::pengRobinson fluid1;
    fluid1.setPressure(p1);
    fluid1.setTemperature(T1);

    // Volumes
    STPC::pengRobinson::phaseType Volume = fluid1.getVolume(p1, T1);
    //double VLiquid = Volume.liquid;
    double VGas    = Volume.gas;
    ui->V1->setNum(VGas);

    // Compressibility
    STPC::pengRobinson::phaseType Compressibility = fluid1.getCompressibility(p1, T1);
    double ZGas    = Compressibility.gas;
    ui->Z1->setNum(ZGas);

    // Enthalpy departure function
    STPC::pengRobinson::phaseType Hdep = fluid1.getDepartureEnthalpy(T1, Compressibility, Volume);
    //double HdepLiquid = Hdep.liquid;
    double HdepGas = Hdep.gas;
    ui->H1->setNum(HdepGas);

    // Entropy departure function
    STPC::pengRobinson::phaseType Sdep = fluid1.getDepartureEntropy(T1, Compressibility, Volume);
    //double HLiquid = Sdep.liquid;
    double SdepGas = Sdep.gas;
    ui->S1->setNum(SdepGas);

    //Calculate new temperature
    T2      = fluid1.getIsentropicProcess(p1, p2, T1);
    ui->T2->setNum(T2);

    int m = 10;
    int n = m + 1;
    STPC::pengRobinson::thermodynamicProperties fluid;
    std::vector<STPC::pengRobinson::thermodynamicProperties> fluidlist;
    std::vector<double> p(n,0);
    p[0] = p1;
    std::vector<double> T(n,0);
    T[0] = T1;

    for (int i = 1; i < n; i++)
    {
        std::cout << "i: " << i << std::endl;
        p[i]            = p1 + (p2-p1)/m*i;
        std::cout << "p: " << p[i] << std::endl;
        T[i]            = fluid1.getIsentropicProcess(p[i-1],p[i],T[i-1]);
        std::cout << "T: " << T[i] << std::endl;
        fluid           = fluid1.calculateAllThermodynamicProperties(p[i],T[i]);
        fluidlist.push_back(fluid);
    }
    STPC::pengRobinson::thermodynamicProperties fluid2;

    fluid2 = fluid1.calculateAllThermodynamicProperties(p2, T2);

    // Volumes
    //double VLiquid2 = fluid2.V.liquid;
    double VGas2   = fluid2.V.gas;
    ui->V2->setNum(VGas2);

    // Compressibility
    //double ZLiquid2 = fluid2.Z.liquid;
    double ZGas2    = fluid2.Z.gas;
    ui->Z2->setNum(ZGas2);

    // Enthalpy departure function
    //double HLiquid2 = fluid2.Hdep.liquid;
    double HdepGas2 = fluid2.Hdep.gas;
    ui->H2->setNum(HdepGas2);

    // Entropy departure function
    //double Sdep2 = fluid2.Sdep.liquid;
    double SdepGas2 = fluid2.Sdep.gas;
    ui->S2->setNum(SdepGas2);

    X = p;
    Y = T;
    QChart *chartToPlot = plotFigure();
    ui->chartView->setChart(chartToPlot);
}
